//
//  AdministrationVC.swift
//  WaterMeter
//
//  Created by petruta maties on 29/11/2019.
//  Copyright © 2019 petruta maties. All rights reserved.
//

import UIKit
import Firebase

class AdministrationVC: UIViewController {

    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var hotWaterToggle: UISwitch!
    @IBOutlet weak var coldWaterPriceTxt: UITextField!
    @IBOutlet weak var hotWaterPriceTxt: UITextField!
    @IBOutlet weak var ronTxt: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hotWaterPriceTxt.isHidden = true
        ronTxt.isHidden = true
  
    }
    

    @IBAction func saveBtnPressed(_ sender: Any) {
        guard let userId = Auth.auth().currentUser?.uid,
//            let coldWaterPrice = coldWaterPriceTxt.text,
            let hotWaterPrice = hotWaterPriceTxt.text,
            let phone = phoneTxt.text else { return }
        
        if hotWaterPriceTxt.isHidden {
        if  phone.isEmpty  {
            let myAlert = UIAlertController(title: "Alert", message: "All fields are required to fill in", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true, completion: nil)
        } else {
            Firestore.firestore().collection("users").document(userId).setData(["phone": phone], merge: true)
            let myAlert = UIAlertController(title: "Alert", message: "The phone number was saved.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            myAlert.addAction(okAction)
            self.present(myAlert, animated: true) {
                self.hotWaterPriceTxt.text = ""
                self.phoneTxt.text = ""
            }
            
            
            }
        } else {
            if phone.isEmpty || hotWaterPrice.isEmpty {
                let myAlert = UIAlertController(title: "Alert", message: "All fields are required to fill in", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                myAlert.addAction(okAction)
                self.present(myAlert, animated: true, completion: nil)
            } else {
                Firestore.firestore().collection("users").document(userId).setData(["coldWaterPrice": hotWaterPrice, "phone": phone], merge: true)
                
                let myAlert = UIAlertController(title: "Alert", message: "The changes were saved.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.dismiss(animated: true, completion: nil)
                }
                myAlert.addAction(okAction)
                self.present(myAlert, animated: true) {
                    self.hotWaterPriceTxt.text = ""
                    self.phoneTxt.text = ""
                }
            }
        }
        
        
//        let myAlert = UIAlertController(title: "Alert", message: "The changes were saved.", preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
//            self.dismiss(animated: true, completion: nil)
//        }
//        myAlert.addAction(okAction)
//        self.present(myAlert, animated: true) {
//            self.hotWaterPriceTxt.text = ""
//            self.phoneTxt.text = ""
//        }
//
        
        
        
        
    }
    

    
        @IBAction func `switch`(_ sender: UISwitch) {
            if sender.isOn == true {
                hotWaterPriceTxt.isHidden = false
                ronTxt.isHidden = false
            } else {
                hotWaterPriceTxt.isHidden = true
                ronTxt.isHidden = true
            }
    
        }
    
}
